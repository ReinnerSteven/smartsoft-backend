#!/usr/bin/env node

/**
 * Module dependencies.
 */


//Aplicacion express
var app = require('../app');
var debug = require('debug')('snip:server');
//Monggose para mongodb
var mongoose = require('mongoose');
var port = normalizePort(process.env.PORT || '3100');
var http = require('http').Server(app);
//websocket con socket.io
var io = require('socket.io')(http);


//Conexion por sockets
io.on('connection', function(socket){
  console.log('a user connected');
    
    socket.on('item', function(msg){
    console.log('message: ' + msg);
  });
    
    
});

//Conectar a la base de datos de mongodb

/*

    Debe estar el servicio de mongodb activo para que se realice la conexion 
*/
mongoose.connect('mongodb://localhost/application', (err,res)=> {
  if(err){
    
    throw err;
  }else{
    console.log("Connect to mongoDB")
    http.listen(port, function(){
      console.log('listening on ' + port);
    });
  }
})













/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
