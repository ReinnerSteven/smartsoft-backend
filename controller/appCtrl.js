'use strict'

var App_DB = require('../models/appModel');

//Get de todas las reiglas
function getAllApp(req,res){
	App_DB.find({}, (err,item_db) =>{
		if(err){
			res.status(500).send({error:err})
		}else{
			if(item_db){
				res.status(200).send({data:item_db})
			}else{
				res.status(404).send({
					error:{_message:"Not found"}
				})	
			}
		}
	})
}

//Get por id de la reigla
function getAppById(req,res){
	var itemId = req.params.id;
	App_DB.findById(itemId, (err,item_db) =>{

		if(err){
			res.status(500).send({error:err})
		}else{

			if(item_db){
				res.status(200).send({data:item_db})
			}else{
				res.status(404).send({
					error:{_message:"Not found"}
				})	
			}
		}
	})
}

//Crear una reigla
function createApp(req,res){

	var app_model = new App_DB();
	var params = req.body    
    app_model.jsonObj = params.jsonObj;
    app_model.name = params.name;
    app_model.condition = params.condition;
 
    var promise = app_model.save();   
    promise.then((item_db)=> {

    	if(item_db){
    		
			res.status(200).send({
				data:item_db
			})
    	}else{
			res.status(404).send({
				error:{_message:"Not save"}
			})	
    	}
    }, (err) => {
		res.status(500).send({
			err
		})	
    });
}

//Actualziar de una reigla
function updateApp(req,res){
	
	var dataId = req.params.id;
	var update = req.body;

	App_DB.findByIdAndUpdate(dataId, update, (err,item_db)=>{
		if(err){
			res.status(500).send({
				err
			})
		}else{

			if(item_db){
				getAppById(req,res);
			}else{
				res.status(404).send({
					error:{_message:"Not update"}
				})	
			}
		}
	})
}

//Eliminar una reigla
function deleteApp(req,res){
	
	var itemId = req.params.id
	App_DB.findByIdAndRemove(itemId, (err,item_db)=>{
		if(err){
			res.status(500).send({
				error:err
			})
		}else{

			if(item_db){
				res.status(200).send({
					user:item_db,
					userDelete: true
				})
			}else{
				res.status(404).send({
					error:{_message:"Not exist"}
				})		
			}
		}
	})
}

module.exports = {
	getAllApp,getAppById,createApp,updateApp,deleteApp
}