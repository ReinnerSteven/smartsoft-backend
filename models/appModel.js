'use strict'

//Cargar mongoose
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var Schema = mongoose.Schema;


//Schema en mongoose
var AppSchema = Schema({
	name:       { type: String, required: true },
    condition:  { type: String, required: true },
    jsonObj:    Schema.Types.Mixed,
    create:     { type: Date, default: Date.now },
    
})

module.exports =  mongoose.model('App_DB',AppSchema)