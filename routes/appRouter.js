'use strict'

const express = require('express');
const AppController = require('../controller/appCtrl');
const api = express.Router();



//Configuracion de las rutas para el servicio REST

api.post('/items', AppController.createApp);
api.get('/items', AppController.getAllApp);
api.get('/items/:id', AppController.getAppById);
api.delete('/items/:id', AppController.deleteApp);
api.put('/items/:id', AppController.updateApp);

module.exports = api